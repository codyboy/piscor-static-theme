## Piscor Static theme 

### VUE STOREFRONT FRONTEND APP

We agreed together with CEO and Marketing Office to develop the frontend on the same basis of websites
we saw (philipstein,almagreendesign), here follow some specifications about the visualizations of pages.

### MOCKS


#### LANDING PAGE HOME
https://www.gruig.com/new/

#### CONTACT PAGE
https://www.gruig.com/new/contacts.html

#### FAQ
https://www.gruig.com/new/faq-categories.html
https://www.gruig.com/new/faq.html

#### ACCOUNT PAGES
https://www.gruig.com/new/account-login.html
https://www.gruig.com/new/account-register.html
https://www.gruig.com/new/account-password-recovery.html

#### CART PAGE
we need a cart page for seo purposes
https://www.gruig.com/new/cart.html

#### CHECKOUT
we like your one page solution but, we want to attach our mockups to better detail what er expect
http://gruig.com/new/checkout-login.html
http://gruig.com/new/checkout-address.html
http://gruig.com/new/checkout-payment.html
https://www.gruig.com/new/checkout.html
http://gruig.com/new/checkout-complete.html

#### CATEGORY PAGE
https://www.gruig.com/new/shop-category.html

#### PRODUCT PAGE
https://www.gruig.com/new/shop-single.html

#### BLOG
https://www.gruig.com/new/magazine-rs.html
https://www.gruig.com/new/magazine-single-rs.html


### REQUESTS

We possibly like to have a preview (a plain low quality jpg file fits perfectly our needs) 
of how you can integrate our visuals on your VUE frontend basis, for at least the homepage, 
the category page with filters and the product page. You have just to consider that:
1 we need a separate page for the cart 

2 the checkout process can be done in the same page but I noticed to you our actual mockups to let you 
better understand what we need in the checkout

3 you have to integrate our fonts, colors and styles in the VUE app  

### repository details

In the root folder of the theme there are the html static files those represent the site's pages and their visualizations.
The said files are named with prefix (account-,shop-, etc) that indicates macro-areas in the layout structure 
to wich the single visualizations belong. In the /components folder you can find elements used in the 
various views.


### our test environment

We have installed the official aimeos/aimeos package, the laravel app plus the aimeos package, on a Centos 7 with:
php72, nginx, mariaDb 10.2.24 and we plan to optimize this configuration just as indicated in the official docs.
We want to use APCu as php cache and redis to cache queries, to split database per instances as orders, customers and products, 
to serve images through cloudflare cdn.
We want an aimeos extension that we can install on various webdomains hosting just the frontend application, client-html package.
At first glance the official package seems to cover all our needs but now we don't know if there are 
controllers to write for the frontend because for the moment we are focusing on the backend side.


#### considerations

Our goal is to leave our actual erp and start using aimeos to manage products, customer, orders and stocks.
Because of the flexibility of the system we think it is possible to proceed like so:

We develop to Connect our actual erp with the aimeos backend via the feeds system and continue to manage orders and archives
on our platform

We develop to have a laravel-aimeos backend app that receives feeds from our pim, serves contents to frontends, 
sends feeds to our pim to integrate users, stocks and orders. We import/export and sync our database for products, stock, 
customers, orders. 
You develop a frontend-VUE-storefront-application on the aimeos app (that will be duplicated on different domains such as piscor.it, piscor.es, etc.), this of course has to be fully detached from the platform, 
then we can continue to develop and test our backend applications gradually 
while already have a fluent and quick frontend app.

Please consider we have partners for the graphic development and we can make further modifications to the frontend even 
by ourselves. You can safely give to us the ready to use vue application skeleton to satisfy the minimum requirements
of a eshop(I mean strictly the pages structure) and then we are able to integrate new modules or components as slideshows, 
banners, ads and so on.